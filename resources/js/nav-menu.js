import { FIRST_PAGE, SECOND_PAGE } from "./routes";
import { AbandonedCartMajor, AccessibilityMajor } from "@shopify/polaris-icons";

export default [{
  items: [
    {
      label: 'First page',
      url: FIRST_PAGE,
      icon: AbandonedCartMajor,
    }, {
      label: 'Second page',
      url: SECOND_PAGE,
      icon: AccessibilityMajor,
    },
  ],
}];
