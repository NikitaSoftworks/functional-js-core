import { useCallback, useEffect, useMemo, useRef } from "react";

/**
 *
 * @return {Object}
 */
export default function useMounted () {
    let mounted = useRef(false);

    useEffect(() => {
        mounted = true;
        return () => {
            mounted = false;
        };
    }, []);

    const ifMounted = useCallback(func => {
        if (mounted) {
            func();
        }
    }, [mounted]);

    return useMemo(() => ({mounted, ifMounted}), [mounted, ifMounted]);
}
