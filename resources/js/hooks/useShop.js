import { useState } from "react";

export default function useShop (props) {
  const [shop, set] = useState(props);

  return {...shop, set};
}