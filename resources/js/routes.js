import FirstPage from "./pages/FirstPage";
import SecondPage from "./pages/SecondPage";

const FIRST_PAGE = '/';
const SECOND_PAGE = '/second';

const PAYMENT = '/payment';

const routes = [{
    component: FirstPage,
    path: FIRST_PAGE,
    exact: true,
}, {
    component: SecondPage,
    path: SECOND_PAGE,
    exact: true,
}];

export { routes, FIRST_PAGE, SECOND_PAGE, PAYMENT };
