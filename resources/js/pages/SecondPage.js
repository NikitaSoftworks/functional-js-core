import React, { useCallback, useContext } from "react";
import { Page, Card, Layout, Stack, Button } from "@shopify/polaris";
import {LoadingContext} from "@spurit/js-core-2";
import {ToastContext} from "@spurit/js-core-2";
import {ModalContext} from "@spurit/js-core-2";
import useMounted from "../hooks/useMounted";
import {ShopContext} from "@spurit/js-core-2";

export default function SecondPage () {
  const loading = useContext(LoadingContext);
  const toggleLoading = useCallback(() => loading.toggle(), [loading.toggle]);

  const toast = useContext(ToastContext);
  const showToast = useCallback(() => toast.show('Hello!'), [toast.show]);

  const modal = useContext(ModalContext);
  const shop = useContext(ShopContext);
  const onModalConfirm = useCallback(() => {
    shop.set({name: 'updated-name'});
    modal.hide();
  }, [shop.set, modal.hide]);

  const showModal = useCallback(() => {
    modal.show('Here is the title', 'Here is the content', {
      primaryAction: {content: 'OK', onAction: onModalConfirm},
    });
  }, [modal.show, onModalConfirm]);

  const mounted = useMounted();
  const showAlertIfMounted = useCallback(() => {
    mounted.ifMounted(() => alert('This alert will be shown only on mounted component'));
  }, [mounted]);

  const startAlertTimeout = useCallback(() => {
    setTimeout(showAlertIfMounted, 2000);
  }, [showAlertIfMounted]);

  return (
    <Page title="Second page">
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <Stack>
              <Button onClick={toggleLoading}>Test loading</Button>
              <Button onClick={showToast}>Test toast</Button>
              <Button onClick={showModal}>Test modal</Button>
            </Stack>
          </Card>
        </Layout.Section>

        <Layout.Section>
          <Card sectioned>
            <Stack vertical>
              <Stack>
                <Button onClick={startAlertTimeout}>Test useMounted</Button>
              </Stack>
            </Stack>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
