import React from "react";
import { Page, Card, Layout, TextContainer } from "@shopify/polaris";

export default function FirstPage () {
    return (
        <Page title="First page">
            <Layout>
                <Layout.Section>
                    <Card sectioned>
                        <TextContainer>Hello</TextContainer>
                    </Card>
                </Layout.Section>
            </Layout>
        </Page>
    );
}
