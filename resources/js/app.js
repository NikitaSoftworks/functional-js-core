import React from "react";
import ReactDOM from 'react-dom';
import navItems from './nav-menu';
import { App } from '@spurit/js-core-2';
import { PAYMENT, routes } from "./routes";
import useShop from "./hooks/useShop";

require('./bootstrap');

const theme = {
  appFrontName: 'Recurring Orders & Subscriptions',
  logo: {
    topBarSource: '/img/app-icon.svg',
    width: 24,
    url: '/',
  },
};

ReactDOM.render(
  <App
    shop={window.Spurit.shop}
    paymentPageUrl={PAYMENT}
    navItems={navItems}
    shopHook={useShop}
    routes={routes}
    theme={theme}
  />,
  document.querySelector('#app'),
);
